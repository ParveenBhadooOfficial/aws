# Hash Hackers Store

[![Logo](https://images.cdn.hashhackers.com/logo/logo-d.svg)](https://store.hashhackers.com)

`Check domain before purchasing. (hashhackers.com and its subdomains only)`

[![Reviews](https://www.cusrev.com/badges/store.hashhackers.com-wl.png)](https://www.cusrev.com/reviews/hashhackers)

## AWS RDP and VPS 4 vCPU 8 GB RAM 1 TB SSD

* OS: Windows Server 2019 for RDP and Ubuntu 20.XX for VPS
* Virtual Cores are limited to 4 vCPU
* RAM is Fixed i.e. 8 GB
* 1 TB SSD GP3 Disk with 3000 IOPS and 150 MBPS throughput
* Open Ports: ALL PORTS ARE OPENED BY DEFAULT
* Price: 7 USD or 500 INR
* Instance Type is c5a.xlarge (Compute Optimized) 2nd generation AMD EPYC 7002 series processors running at frequencies up to 3.3 GHz
* upto 10 GBPS Port
* SSH Key for Ubuntu
* Login username and password for Windows

## AWS RDP (Windows Only) 4 vCPU 30.5 GB RAM 1 TB SSD

* OS: Windows Server 2019 for RDP
* Virtual Cores are limited to 4 vCPU
* RAM is Fixed i.e. 30.5 GB
* C DRIVE with 200 GB SSD GP3 Disk with 3000 IOPS and 150 MBPS throughput
* D DRIVE with 950 GB NVMe SSD (Super Fast Disks)
* Open Ports: ALL PORTS ARE OPENED BY DEFAULT
* Price: 7 USD or 500 INR
* Instance Type is i3.xlarge (Storage Optimized) High Frequency Intel Xeon E5-2686 v4 (Broadwell) Processors with base frequency of 2.3 GHz
* upto 10 GBPS Port
* Login username and password for Windows

## AWS RDP and VPS 8 vCPU 16 GB RAM 2 TB SSD

* OS: Windows Server 2019 for RDP and Ubuntu 20.XX for VPS
* Virtual Cores are limited to 8 vCPU
* RAM is Fixed i.e. 16 GB
* 2 TB SSD GP3 Disk with 3000 IOPS and 150 MBPS throughput
* Open Ports: ALL PORTS ARE OPENED BY DEFAULT
* Price: 14.50 USD or 999 INR
* Instance Type is c5a.2xlarge (Compute Optimized) 2nd generation AMD EPYC 7002 series processors running at frequencies up to 3.3 GHz
* upto 10 GBPS Port
* SSH Key for Ubuntu
* Login username and password for Windows

## AWS RDP (Windows Only) 8 vCPU 64 GB RAM 5.2 TB  Total SSD

* OS: Windows Server 2019 for RDP
* Virtual Cores are limited to 8 vCPU
* RAM is Fixed i.e. 64 GB
* C DRIVE with 200 GB SSD GP3 Disk with 3000 IOPS and 150 MBPS throughput
* D DRIVE with 2.5 TB NVMe SSD (Super Fast Disks)
* E DRIVE with 2.5 TB NVMe SSD (Super Fast Disks)
* Open Ports: ALL PORTS ARE OPENED BY DEFAULT
* Price: 14.50 USD or 999 INR
* Instance Type is i3en.2xlarge (Storage Optimized) Up to 3.1 GHz Intel® Xeon® Scalable (Skylake) processors with new Intel Advanced Vector Extension (AVX-512) instruction set
* upto 25 GBPS Port
* Login username and password for Windows

## Terms of Service and More Information about AWS Servers

* All Purchases are delivered within 24-48 hours, in rare cases may take extra time, in cases such as No Electricity.
* Purchase is valid for 30 days or 1 month from same date from 1st month to 2nd month, eg. if you purchase on 5th of June, its valid till 5th of July.
* User needs to provide Telegram Username for contacts.
* Admin will contact user before or after the due date for renewal.
* Server will not be deleted after 30 days, you'll be given a chance to renew.
* Server are supposed to run for 1 month only, sometimes they could run for 2 month or 1.5 months or any tenure more than 1 month.
* When the server stops working, we'll provide replacement.
* We do not guarantee data recovery or backups for your server, user must backup their work every few days.
* We may change server inbetween any time of month if we know the server is about to die, we'll backup your C Drive incase of Windows and Whole disk incase of Ubuntu and transfer the data to new account.
* In very rare cases Server may die before 1 month of creation, in this case we'll provide new server and will extend 1 day in your billing.
* We do not allow Crpto Mining on these products.
* You can ENCODE Videos, but we cannot provide performace reports for ENCODING because that depends on CPU.
* We do not provide GPU Servers.
* We do not provide servers bigger than 8 vCPU.
* While the bandwidth is Unlimited, if you use excess bandwidth, server may die faster. We'll still provide replacements.
* You can seed Torrents, but that uses a lot of bandwidth so you'll be responsible for early death of server. We'll still provide replacements.
* We may decline renewal incase of shortage of Stock. (in rare cases)
* We have 40+ Active users for these servers. We have 4.5 Star store ratings at [CusRev](https://www.cusrev.com/reviews/hashhackers/p/p-9416)
* We'll try to help you as much as we can.
* Payments Details will be provided when contacted.

[![Contact](https://i1.wp.com/store.hashhackers.com/contact-us-via-telegram.png)](https://telegram.dog/TheFirstSpeedster)
